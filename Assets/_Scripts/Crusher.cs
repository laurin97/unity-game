using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Crusher : MonoBehaviour
{
    public GameObject top;
    public GameObject bottom;
    private Transform currentPoint;

    public float downSpeed;
    public float upSpeed;

    private void Start()
    {
        transform.position = top.transform.position;
        currentPoint = bottom.transform;
    }

    private void Update()
    {
        Vector2 point = currentPoint.position - transform.position;
        if (currentPoint == bottom.transform)
        {
            transform.position = Vector2.MoveTowards(this.transform.position, bottom.transform.position, downSpeed * Time.deltaTime);
        }
        else if (currentPoint == top.transform)
        {
            transform.position = Vector2.MoveTowards(this.transform.position, top.transform.position, upSpeed * Time.deltaTime);
        }

        if (Vector2.Distance(transform.position, currentPoint.position) < 0.5f && currentPoint == bottom.transform)
        {
            currentPoint = top.transform;
        }
        else if (Vector2.Distance(transform.position, currentPoint.position) < 0.5f && currentPoint == top.transform)
        {
            currentPoint = bottom.transform;
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Player")
            GameObject.FindWithTag("Player").GetComponent<Player>().takeDamage(100);
    }
}
