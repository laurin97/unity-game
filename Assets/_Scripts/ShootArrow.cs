using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShootArrow : MonoBehaviour
{
    public GameObject arrow;
    public Transform shotPoint;
    float timer;
    public float firerate;

    void Update()
    {
        if(timer >= firerate)
        {
            GameObject newArrow = Instantiate(arrow, shotPoint.position, shotPoint.rotation);
            newArrow.transform.eulerAngles = new Vector3(0, 0, 180);
            newArrow.GetComponent<arrowScript>().arrowVelocity = 50;
            timer = 0;
        }
        timer += Time.deltaTime;
    }
}
