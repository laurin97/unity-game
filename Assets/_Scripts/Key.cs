using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Key : MonoBehaviour
{
    public GameObject pickup;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Player")
        {
            pickup.GetComponent<Animator>().SetTrigger("pickup");
            GameObject.Find("Dungeon_door").GetComponent<Dungeon_2>().keypickedup = true;
            gameObject.SetActive(false);
            pickup.SetActive(false);
        }
    }
}