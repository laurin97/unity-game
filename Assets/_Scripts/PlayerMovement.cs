using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{

    public CharacterController2D controller;
    public Animator animator;
    public Player player;
    private new AudioSource audio;
    public AudioClip jumpSound;

    public float runSpeed = 40f;
    float horizontalMove = 0f;
    bool jump = false;
    public bool isJumping = false;
    public float slideSpeedAmount;
    private float slideSpeed;
    public State state;
    public bool isAttacking;
    public bool hittingWall;
    private float timer;
    public float rollTimer;

    public enum State
    {
        Normal,
        DodgeRollSliding,
    }
    public Animator playerAnimator;

    private void Awake()
    {
        audio = GetComponent<AudioSource>();
        state = State.Normal;
        timer = rollTimer;
    }

    // Update is called once per frame
    void Update()
    {
        timer += Time.deltaTime;
        
        switch (state)
        {
            case State.Normal:
                
                horizontalMove = Input.GetAxisRaw("Horizontal") * runSpeed;
                animator.SetFloat("Speed", Mathf.Abs(horizontalMove));

                if (Input.GetKeyDown(KeyCode.W) && isAttacking == false)
                {
                    audio.clip = jumpSound;
                    audio.Play();
                    jump = true;
                    animator.SetBool("IsJumping", true);
                    isJumping = true;
                }

                dodgeroll();
                
                break;

            case State.DodgeRollSliding:
                horizontalMove = 0;
                DodgeRollSliding();
                Debug.Log(slideSpeed);
                break;
        }
    }

    public void OnLanding ()
    {
        animator.SetBool("IsJumping", false);
        isJumping = false;
    }

    void FixedUpdate ()
    {
        // move character
        
        if(state == State.Normal && player.isDead == false)
        {
            if (isAttacking == true)
            {
                controller.Move(0 * Time.fixedDeltaTime, false, jump);
            }
            else
            {
                controller.Move(horizontalMove * Time.fixedDeltaTime, false, jump);
            }
        }
        jump = false;
    }

    private void dodgeroll ()
    {
        if (Input.GetKeyDown(KeyCode.S) && !isJumping && !isAttacking && timer >= rollTimer)
        {
            audio.clip = jumpSound;
            audio.Play();
            timer = 0;
            controller.Move(0 * Time.fixedDeltaTime, false, jump);
            slideSpeed = slideSpeedAmount;
            state = State.DodgeRollSliding;
            animator.SetTrigger("roll");
            //animator.SetBool("IsRolling", true);
        }
    }

    private void DodgeRollSliding()
    {

        switch (controller.m_FacingRight)
        {
            case true:
                controller.Move(0 * Time.fixedDeltaTime, false, jump);
                transform.position += new Vector3(0.1f, 0) * slideSpeed * Time.deltaTime;
                break;

            case false:
                controller.Move(0 * Time.fixedDeltaTime, false, jump);
                transform.position += new Vector3(-0.1f, 0) * slideSpeed * Time.deltaTime;
                break;
        }
        slideSpeed -= slideSpeed * 10 * Time.deltaTime;

        if (slideSpeed < 5 || hittingWall)
        {
            state = State.Normal;
            animator.SetBool("IsRolling", false);
            slideSpeed = slideSpeedAmount;
        }
    }

    public void stopAttacking()
    {
        playerAnimator.SetBool("IsAttacking", false);
        isAttacking = false;
    }
}
