using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Boss_Run : StateMachineBehaviour
{
    Transform player;
    Rigidbody2D rb;
    Boss boss;

    public float attackRange = 3;
    public float speed = 1;
    public float dashTimer;
    public float attackRate;
    private float timer;
    private float attackTimer;
    private float attackCounter;

    // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        player = GameObject.FindWithTag("Player").transform;
        rb = animator.GetComponent<Rigidbody2D>();
        boss = animator.GetComponent<Boss>();
    }

    // OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        boss.LookAtPlayer();
        timer += Time.fixedDeltaTime;
        Vector2 target = new Vector2(player.position.x, rb.position.y);
        Vector2 newPos = Vector2.MoveTowards(rb.position, target, speed * Time.fixedDeltaTime);
        rb.MovePosition(newPos);
        attackTimer += Time.fixedDeltaTime;


        if (Vector2.Distance(player.position, rb.position) <= attackRange && attackTimer >= attackRate && attackCounter < 2)
        {
            attackTimer = 0;
            animator.SetTrigger("attack");
            attackCounter++;
        }

        if (Vector2.Distance(player.position, rb.position) <= attackRange && attackTimer >= attackRate && attackCounter >= 2)
        {
            attackCounter = 0;
            animator.SetTrigger("attack2");
        }

        if (timer >= dashTimer)
        {
            animator.SetBool("dashing", true);
        }
    }

    // OnStateExit is called when a transition ends and the state machine finishes evaluating this state
    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        animator.ResetTrigger("attack");
        timer = 0;
    }
}
