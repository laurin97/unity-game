using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Mill : MonoBehaviour
{
    public Animator transition;

    // Update is called once per frame
    void Update()
    {
        if (isCurrentlyColliding == true && Input.GetKeyDown(KeyCode.E))
        {

            Scene scene = SceneManager.GetActiveScene();

            if (scene.name == "Forest")
            {
                StartCoroutine(loadScene("Mill"));
            }
            else
            {
                StartCoroutine(loadScene("Forest"));
            }
        }
    }

    bool isCurrentlyColliding;

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Player")
        {
            isCurrentlyColliding = true;
        }
    }

    private void OnTriggerExit2D(Collider2D other)
    {
        if (other.tag == "Player")
        {
            isCurrentlyColliding = false;
        }
    }

    IEnumerator loadScene(string sceneName)
    {
        transition.SetTrigger("Start");
        yield return new WaitForSeconds(1);
        SceneManager.LoadScene(sceneName);
    }
}
