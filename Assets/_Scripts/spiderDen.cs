using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class spiderDen : MonoBehaviour
{
    public Animator anim;
    public GameObject spider;
    private SpiderAI scrpt;
    public GameObject point1;
    public GameObject point2;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.tag == "Player")
        {
            anim.SetTrigger("open");
        }
    }

    public void SpawnSpider()
    {
        var clone = Instantiate(spider);
        clone.transform.position = transform.position;
        scrpt = clone.GetComponent<SpiderAI>();
    }
}
