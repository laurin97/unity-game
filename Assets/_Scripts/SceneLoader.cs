using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneLoader : MonoBehaviour
{
    private GameObject interactText;
    private GameObject player;
    public Animator transition;

    // Start is called before the first frame update
    void Start()
    {
        interactText = GameObject.FindWithTag("interactText");
        player = GameObject.FindWithTag("Player");
    }

    // Update is called once per frame
    void Update()
    {
        if (isCurrentlyColliding == true && Input.GetKeyDown(KeyCode.E))
        {
            Scene scene = SceneManager.GetActiveScene();

            if (scene.name == "Forest")
            {
                player.GetComponent<Player>().SetHealthToMax();
                StartCoroutine(loadScene("Cave"));
            }
            else
            {
                StartCoroutine(loadScene("Forest"));
            }
        }
    }

    bool isCurrentlyColliding;

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Player")
        {
            isCurrentlyColliding = true;
        }
    }

    private void OnTriggerExit2D(Collider2D other)
    {
        if (other.tag == "Player")
        {
            isCurrentlyColliding = false;
        }
    }

    IEnumerator loadScene(string sceneName)
    {
        transition.SetTrigger("Start");
        yield return new WaitForSeconds(1);
        SceneManager.LoadScene(sceneName);
    }
}
