using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WallCheck : MonoBehaviour
{
    public GameObject player;

    private void OnCollisionEnter2D(Collision2D collision)
    {
        player.GetComponent<PlayerMovement>().hittingWall = true;
    }
    private void OnCollisionExit2D(Collision2D collision)
    {
        player.GetComponent<PlayerMovement>().hittingWall = false;
    }
}
