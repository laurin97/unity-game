using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Lever : MonoBehaviour
{
    private bool leverDown = false;
    public DungeonDoor door;
    public Animator anim;

    private void OnTriggerEnter2D(Collider2D other) 
    {
        if (other.tag == "arrow" && leverDown == false)
        {
            PullLever();
        }
    }

    public void PullLever()
    {
        StartCoroutine(openDoor());
    }

    private IEnumerator openDoor()
    {
        leverDown = true;
        anim.SetTrigger("Lever");
        anim.SetBool("LeverPulled", true);
        yield return new WaitForSeconds(1);
        door.Open();
    }
}
