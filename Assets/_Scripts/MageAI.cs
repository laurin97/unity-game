using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MageAI : MonoBehaviour
{
    private State state;

    public GameObject p1;
    public GameObject p2;
    public Rigidbody2D rb;
    public Animator anim;
    private GameObject player;
    public Enemy enemy;
    private Transform currentPoint;

    public float chasingSpeed;
    public float speed;
    private float distance;
    public bool flip;
    public float chasingRange;
    public float teleportRange;
    public float attackingRange;
    public float attackRate = 1;
    float nextAttackTime = 0;
    float teleportTimer;
    public float teleportRate;

    private enum State
    {
        patroling,
        chasing,
        attacking,
        teleporting,
    }

    private void Awake()
    {
        player = GameObject.FindWithTag("Player");
        state = State.patroling;
        currentPoint = p1.transform;
    }

    private void Update()
    {
        manageEnemyState();

        checkCurrentStatus();

        switch (state)
        {
            case State.patroling:

                if (enemy.IsDead == false)
                {
                    Flip();

                    anim.SetBool("IsRunning", true);

                    Vector2 point = currentPoint.position - transform.position;
                    if (currentPoint == p2.transform)
                    {
                        transform.position = Vector2.MoveTowards(this.transform.position, p2.transform.position, speed * Time.deltaTime);
                    }
                    else if (currentPoint == p1.transform)
                    {
                        transform.position = Vector2.MoveTowards(this.transform.position, p1.transform.position, speed * Time.deltaTime);
                    }

                    if (Vector2.Distance(transform.position, currentPoint.position) < 0.5f && currentPoint == p2.transform)
                    {
                        Flip();
                        currentPoint = p1.transform;
                    }
                    else if (Vector2.Distance(transform.position, currentPoint.position) < 0.5f && currentPoint == p1.transform)
                    {
                        Flip();
                        currentPoint = p2.transform;
                    }
                }

                break;

            case State.chasing:

                if (enemy.IsDead == false)
                {
                    anim.SetBool("IsRunning", true);
                    distance = Vector2.Distance(transform.position, player.transform.position);
                    Vector2 position = player.transform.position - transform.position;
                    transform.position = Vector2.MoveTowards(this.transform.position, player.transform.position, chasingSpeed * Time.deltaTime);

                    Vector3 scale = transform.localScale;

                    if (player.transform.position.x > transform.position.x)
                    {
                        scale.x = Mathf.Abs(scale.x) * -1 * (flip ? -1 : 1);
                    }
                    else
                    {
                        scale.x = Mathf.Abs(scale.x) * (flip ? -1 : 1);
                    }

                    transform.localScale = scale;
                    teleportTimer += Time.deltaTime;
                    if(teleportTimer >= teleportRate)
                    {
                        teleportTimer = 0;
                        anim.SetTrigger("teleport");
                    }
                }

                break;

            case State.attacking:

                if (enemy.IsDead == false)
                {
                    anim.SetBool("IsRunning", false);
                    Vector3 scale = transform.localScale;

                    if (Time.time >= nextAttackTime)
                    {
                        anim.SetTrigger("attack");
                        nextAttackTime = Time.time + 1 / attackRate;
                    }

                    if (player.transform.position.x > transform.position.x)
                    {
                        scale.x = Mathf.Abs(scale.x) * -1 * (flip ? -1 : 1);
                    }
                    else
                    {
                        scale.x = Mathf.Abs(scale.x) * (flip ? -1 : 1);
                    }
                }

                break;
        }
    }

    private void Flip()
    {
        Vector3 scale = transform.localScale;


        if (currentPoint.transform.position.x > transform.position.x)
        {
            scale.x = Mathf.Abs(scale.x) * -1 * (flip ? -1 : 1);
        }

        else
        {
            scale.x = Mathf.Abs(scale.x) * (flip ? -1 : 1);
        }

        transform.localScale = scale;
    }

    private void manageEnemyState()
    {
        distance = Vector2.Distance(transform.position, player.transform.position);

        if (distance < chasingRange)
        {
            state = State.chasing;

            if (distance < attackingRange)
                state = State.attacking;
        }
        else if (distance > teleportRange)
            state = State.patroling;

        //else if (distance > chasingRange && distance < teleportRange)
            //state = State.teleporting;
    }

    private void checkCurrentStatus()
    {
        if (Input.GetKeyDown(KeyCode.Alpha5))
        {
            Debug.Log(currentPoint);
            Debug.Log(state);
        }
    }

    public void Teleport()
    {
        transform.position = player.transform.position;
        anim.SetTrigger("appear");
    }
}
