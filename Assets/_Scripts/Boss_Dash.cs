using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Boss_Dash : StateMachineBehaviour
{
    Transform player;
    Rigidbody2D rb;
    Boss boss;

    public float dashSpeed;
    public float dashDuration;
    private float timer;
    public float damageInterval;
    Vector2 newPos;
    Vector2 target;

    // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        boss = animator.GetComponent<Boss>();
        player = GameObject.FindWithTag("Player").transform;
        rb = animator.GetComponent<Rigidbody2D>();
        boss.dealDashDamage = true;
        if(player.position.x < rb.position.x)
        {
            target = new Vector2(rb.position.x - 20, rb.position.y);
        }
        else if(player.position.x > rb.position.x)
        {
            target = new Vector2(rb.position.x + 20, rb.position.y);
        }
    }

    // OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        newPos = Vector2.MoveTowards(rb.position, target, dashSpeed * Time.fixedDeltaTime);
        rb.MovePosition(newPos);
        timer += Time.fixedDeltaTime;
        boss.dealDamageDash();
        
        if (timer >= dashDuration)
        {
            animator.SetBool("dashing", false);
        }
    }

    // OnStateExit is called when a transition ends and the state machine finishes evaluating this state
    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        timer = 0;
    }
}
