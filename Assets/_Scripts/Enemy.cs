using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    public Transform attackPosition;
    public LayerMask whatIsPlayer;
    public Animator anim;
    public GameObject impact;
    private ImpactScript ImpactScript;
    public Rigidbody2D rb;
    public GameObject healthBar;
    public GameObject door;
    public new AudioSource audio;
    public AudioClip hitSound;

    public int MaxHealth = 100;
    int currentHealth;
    public int enemyDamage;
    public float attackRange;
    public bool IsDead = false;
    public bool openDoor;

    // Start is called before the first frame update
    void Start()
    {
        audio = GetComponent<AudioSource>();
        currentHealth = MaxHealth;
        healthBar.GetComponent<HealthBar>().setMaxHealth(currentHealth);
        ImpactScript = impact.GetComponent<ImpactScript>();
        this.gameObject.SetActive(true);
    }

    public void dealDamage()
    {
        Collider2D[] hitPlayer = Physics2D.OverlapCircleAll(attackPosition.position, attackRange, whatIsPlayer);

        foreach (Collider2D player in hitPlayer)
        {
            player.GetComponent<Player>().takeDamage(enemyDamage);
        }
    }

    public void takeDamage(int damage)
    {
        if (!IsDead)
        {
            audio.clip = hitSound;
            audio.Play();
            currentHealth -= damage;
            ImpactScript.hurt = true;
            healthBar.GetComponent<HealthBar>().setHealth(currentHealth);

            if (currentHealth <= 0)
            {
                die();
            }
        }
    }

    void die()
    {
        if (openDoor)
            door.GetComponent<DungeonDoor>().enemyCounter++;

        IsDead = true;
        anim.SetTrigger("dead");
        rb.constraints = RigidbodyConstraints2D.FreezeAll;
    }

    public void destroyEnemy()
    {
        this.gameObject.SetActive(false);
    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(attackPosition.position, attackRange);
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.transform.tag == "spikes")
            die();
    }
}
