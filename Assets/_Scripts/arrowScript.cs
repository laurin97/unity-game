using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class arrowScript : MonoBehaviour
{
    public Rigidbody2D rb2d;
    public float arrowVelocity;
    int lifeTime = 3;
    private bool dealDamage = true;

    // Start is called before the first frame update
    void Start()
    {
        rb2d.velocity = transform.right * arrowVelocity;
    }

    private void Awake()
    {
        Destroy(gameObject, lifeTime);
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        rb2d.velocity = Vector2.zero;
        rb2d.isKinematic = true;

        if (collision.transform.tag == "enemy" && dealDamage)
        {
            collision.transform.GetComponent<Enemy>().takeDamage(30);
            Destroy(gameObject);
        }
        else if (collision.transform.tag == "Player" && dealDamage)
        {
            collision.transform.GetComponent<Player>().takeDamage(50);
            Destroy(gameObject);
        }
        else if (collision.transform.tag == "boss" && dealDamage)
        {
            collision.transform.GetComponent<Boss>().takeDamage(30);
            Destroy(gameObject);
        }
        else
            dealDamage = false;
    }
}
