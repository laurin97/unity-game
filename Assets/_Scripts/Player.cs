using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Player : MonoBehaviour
{
    public Animator anim;
    public GameObject healthBar;
    public PlayerMovement movement;
    public GameObject interactText;
    public GameObject defeat;
    public SimpleFlash flashEffect;
    private new AudioSource audio;
    public AudioClip hitAudio;

    public int MaxHealth = 100;
    public int currentHealth;
    bool isColliding = false;
    public bool isDead = false;
    public float bossActivateRange;

    private static Player playerInstance;
    void Awake()
    {
        audio = GetComponent<AudioSource>();
        SetHealthToMax();
        DontDestroyOnLoad(this);
        defeat.SetActive(false);

        if (playerInstance == null)
        {
            playerInstance = this;
        }
        else
        {
            Object.Destroy(gameObject);
        }
    }
    public void takeDamage(int damage)
    {

        if (movement.state == PlayerMovement.State.Normal)
        {
            audio.clip = hitAudio;
            audio.Play();
            currentHealth -= damage;
            changeColor();
        }

        healthBar.GetComponent<HealthBar>().setHealth(currentHealth);

        if (currentHealth <= 0 && !isDead)
        {
            die();
        }
    }

    private void Update()
    {
        if (!isColliding)
            interactText.SetActive(false);        
    }

    void die()
    {
        anim.SetTrigger("dead");
        isDead = true;
        GetComponent<Rigidbody2D>().constraints = RigidbodyConstraints2D.FreezeAll;
    }

    public void disable()
    {
        defeat.SetActive(true);
        GetComponent<Rigidbody2D>().constraints = RigidbodyConstraints2D.FreezeRotation;
    }

    private void changeColor()
    {
        flashEffect.Flash();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.transform.tag == "interactable")
        {
            interactText.SetActive(true);
            isColliding = true;
        }
        else if (collision.transform.tag == "disableText")
            interactText.SetActive(false);
    }
    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.transform.tag == "interactable")
        {
            interactText.SetActive(false);
            isColliding = false;
        }
    }

    public void RestartScene()
    {
        Scene scene = SceneManager.GetActiveScene();
        SceneManager.LoadScene(scene.name);
        SetHealthToMax();
        isDead = false;
        defeat.SetActive(false);
    }

    public void HomeScene()
    {
        Scene scene = SceneManager.GetActiveScene();
        SceneManager.LoadScene("Forest");
        SetHealthToMax();
        isDead = false;
        defeat.SetActive(false);
    }

    public void SetHealthToMax()
    {
        currentHealth = MaxHealth;
        healthBar.GetComponent<HealthBar>().setMaxHealth(currentHealth);
    }
}
