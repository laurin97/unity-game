using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Dungeon_2 : MonoBehaviour
{
    private GameObject interactText;
    public Animator transition;
    public SpriteRenderer rend;
    public Sprite openedDoor;
    public bool doorOpen = false;
    public bool keypickedup = false;
    bool isCurrentlyColliding;
    private GameObject player;

    // Start is called before the first frame update
    void Start()
    {
        interactText = GameObject.FindWithTag("interactText");
        gameObject.GetComponent<Collider2D>().enabled = false;
        player = GameObject.FindWithTag("Player");
    }

    // Update is called once per frame
    void Update()
    {
        if(keypickedup)
            gameObject.GetComponent<Collider2D>().enabled = true;

        if (isCurrentlyColliding == true && Input.GetKeyDown(KeyCode.E) && keypickedup)
        {
            Scene scene = SceneManager.GetActiveScene();

            if (scene.name == "Dungeon")
            {
                rend.sprite = openedDoor;
                player.GetComponent<Player>().SetHealthToMax();
                StartCoroutine(loadScene("Dungeon_2"));
            }
            else
            {
                StartCoroutine(loadScene("Dungeon"));
            }
        }
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Player" && keypickedup)
        {
            isCurrentlyColliding = true;
        }
    }

    private void OnTriggerExit2D(Collider2D other)
    {
        if (other.tag == "Player")
        {
            isCurrentlyColliding = false;
        }
    }

    IEnumerator loadScene(string sceneName)
    {
        transition.SetTrigger("Start");

        yield return new WaitForSeconds(1);

        SceneManager.LoadScene(sceneName);
    }
}
