using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpikeDamage : MonoBehaviour
{   
    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.tag == "Player")
            collision.GetComponent<Player>().takeDamage(100);
        else if (collision.tag == "enemy")
            collision.GetComponent<Enemy>().takeDamage(100);
    }
}
