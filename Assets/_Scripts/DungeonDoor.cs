using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DungeonDoor : MonoBehaviour
{
    public Transform openPoint;
    public Transform closePoint;
    public float openSpeed;
    private bool open = false;
    private bool close = false;
    public int enemyCounter = 0;

    private void Update()
    {
        if (open && transform.position != openPoint.position)
            transform.position = Vector2.MoveTowards(this.transform.position, openPoint.position, openSpeed * Time.deltaTime);
        else if (close && transform.position != closePoint.position)
            transform.position = Vector2.MoveTowards(this.transform.position, closePoint.position, openSpeed * Time.deltaTime);
        else if (enemyCounter >= 3)
            Open();
    }

    public void Open()
    {
        open = true;
    }
    public void Close()
    {
        open = false;
        close = true;
    }
}
