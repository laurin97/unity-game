using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class HouseScript : MonoBehaviour
{
    private GameObject interactText;
    public Animator transition;
    bool isCurrentlyColliding;

    // Start is called before the first frame update
    void Start()
    {
        interactText = GameObject.FindWithTag("interactText");
    }

    // Update is called once per frame
    void Update()
    {
        if (isCurrentlyColliding == true && Input.GetKeyDown(KeyCode.E))
        {
            Scene scene = SceneManager.GetActiveScene();

            if (scene.name == "Forest")
            {
                StartCoroutine(loadScene("House"));
            }
            else
            {
                StartCoroutine(loadScene("Forest"));
            }
        }
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Player")
        {
            isCurrentlyColliding = true;
        }
    }

    private void OnTriggerExit2D(Collider2D other)
    {
        if (other.tag == "Player")
        {
            isCurrentlyColliding = false;
        }
    }

    IEnumerator loadScene(string sceneName)
    {
        transition.SetTrigger("Start");
        yield return new WaitForSeconds(1);
        SceneManager.LoadScene(sceneName);
    }
}
