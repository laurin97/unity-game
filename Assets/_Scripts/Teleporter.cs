using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Teleporter : MonoBehaviour
{
    private GameObject player;
    private GameObject teleportPoint;

    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.FindWithTag("Player");
        teleportPoint = GameObject.FindWithTag("teleportPoint");
        teleportPlayer();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void teleportPlayer()
    {
        player.transform.position = teleportPoint.transform.position;
    }
}
