using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationManager : MonoBehaviour
{
    public Animator animator;

    int WeaponNumber = 2;
    int index;


    private void Start()
    {
        animator.SetLayerWeight(2, 1);
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Q))
        {
            if (WeaponNumber == 2)
            {
                WeaponNumber = 1;
            }
            else
            {
                WeaponNumber = 2;
            }

            ChangeWeapon();
        }
    }

    void ChangeWeapon()
    {
        switch (WeaponNumber)
        {
            case 1:
                animator.SetLayerWeight(1, 1);
                animator.SetLayerWeight(2, 0);
                break;

            case 2:
                animator.SetLayerWeight(2, 1);
                animator.SetLayerWeight(1, 0);
                break;

            


        }

    }
}
