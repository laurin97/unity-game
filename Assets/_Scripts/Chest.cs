using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Chest : MonoBehaviour
{
    public Animator anim;
    public GameObject Key;

    private void Start()
    {
        Key.SetActive(false);
    }

    public void OpenChest()
    {
        anim.SetTrigger("openChest");
    }

    public void DropKey()
    {
        Key.SetActive(true);
    }
}
