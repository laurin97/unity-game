using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Boss : MonoBehaviour
{

	private GameObject player;
	public Transform attackPosition;
	public GameObject impact;
	public GameObject healthBar;
	public Transform dashAttackPoint;
	public GameObject winScreen;
	public LayerMask whatIsPlayer;
	public Rigidbody2D rb;
	private new AudioSource audio;
	public AudioClip hitSound;

	public int dashDamage;
	public float dashRange;
	public bool isFlipped = false;
	public float attackRange;
	public int bossDamage;
	private int currentHealth;
	public int maxHealth;
	private bool isDead = false;
	private float distance;
	public float activateHealthbarRange;
	public bool dealDashDamage = false;

    private void Start()
    {
		winScreen.SetActive(false);
		player = GameObject.FindWithTag("Player");
		healthBar.GetComponent<HealthBar>().setMaxHealth(maxHealth);
		healthBar.SetActive(false);
		currentHealth = maxHealth;
		audio = GetComponent<AudioSource>();
	}

	private void Update()
    {
		distance = Vector2.Distance(transform.position, player.transform.position);
		
		if (distance <= activateHealthbarRange)
        {
			healthBar.SetActive(true);
			GetComponent<Animator>().SetTrigger("activate");
        }
	}

	public void LookAtPlayer()
	{
		Vector3 flipped = transform.localScale;
		flipped.z *= -1f;

		if (transform.position.x > player.transform.position.x && isFlipped)
		{
			transform.localScale = flipped;
			transform.Rotate(0f, 180f, 0f);
			isFlipped = false;
		}
		else if (transform.position.x < player.transform.position.x && !isFlipped)
		{
			transform.localScale = flipped;
			transform.Rotate(0f, 180f, 0f);
			isFlipped = true;
		}
	}

	public void dealDamage()
	{
		Collider2D[] hitPlayer = Physics2D.OverlapCircleAll(attackPosition.position, attackRange, whatIsPlayer);

		foreach (Collider2D player in hitPlayer)
		{
			player.GetComponent<Player>().takeDamage(bossDamage);
		}
	}

	public void dealDamageDash()
	{
        if (dealDashDamage)
        {
			Collider2D[] hitPlayer = Physics2D.OverlapCircleAll(dashAttackPoint.position, dashRange, whatIsPlayer);

			foreach (Collider2D player in hitPlayer)
			{
				player.GetComponent<Player>().takeDamage(dashDamage);
				dealDashDamage = false;
			}
		}
	}

	public void takeDamage(int damage)
	{
		if (!isDead)
		{
			audio.clip = hitSound;
			audio.Play();
			currentHealth -= damage;
			impact.GetComponent<ImpactScript>().hurt = true;
			healthBar.GetComponent<HealthBar>().setHealth(currentHealth);

			if (currentHealth <= 0)
			{
				die();
			}
		}
	}

	void die()
	{
		isDead = true;
		GetComponent<Animator>().SetTrigger("dead");
		GetComponent<Rigidbody2D>().constraints = RigidbodyConstraints2D.FreezeAll;
	}

	private void OnDrawGizmosSelected()
	{
		Gizmos.color = Color.red;
		Gizmos.DrawWireSphere(attackPosition.position, attackRange);
		Gizmos.DrawWireSphere(dashAttackPoint.position, dashRange);
	}

	public void Win()
    {
		winScreen.SetActive(true);
    }

	public void RestartScene()
	{
		player.GetComponent<Player>().RestartScene();
	}

	public void HomeScene()
	{
		player.GetComponent<Player>().HomeScene();
	}
}
