using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAttack : MonoBehaviour
{
    public Transform attackPosition;
    public LayerMask whatIsEnemie;
    public Animator playerAnim;
    public PlayerMovement movementScript;
    public AnimationManager animManager;
    public GameObject arrow;
    public Transform shotPoint;
    public CharacterController2D characterController;
    public AudioClip bowAudio;
    public AudioClip swordAudio;
    private new AudioSource audio;

    public float attackRange;
    private int attackNumber = 1;
    public float launchForce;
    public int swordDamage;
    private float bowCounter;
    public float shootRate;

    private void Start()
    {
        audio = GetComponent<AudioSource>();
    }

    private void Update()
    {
        bowCounter += Time.deltaTime;
        if(bowCounter >= shootRate)
        {
            bowCounter = 0;
            playerAnim.SetBool("canShoot", true);
        }

        if (Input.GetKeyDown(KeyCode.Space) && movementScript.isJumping == false && movementScript.isAttacking == false)
        {
            if (attackNumber <= 3)
            {
                Attack();
            }
            else
            {
                attackNumber = 1;
                Attack();
            }
        }
    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(attackPosition.position, attackRange);
    }

    public void dealDamage()
    {
        Collider2D[] hitEnemies = Physics2D.OverlapCircleAll(attackPosition.position, attackRange, whatIsEnemie);

        foreach (Collider2D enemy in hitEnemies)
        {
            if (enemy.tag == "enemy")
                enemy.GetComponent<Enemy>().takeDamage(swordDamage);

            else if (enemy.tag == "boss")
                enemy.GetComponent<Boss>().takeDamage(swordDamage);

            else if (enemy.tag == "lever")
                enemy.GetComponent<Lever>().PullLever();

            else if (enemy.tag == "chest")
                enemy.GetComponent<Chest>().OpenChest();
        }
    }

    void shootArrow()
    {
        GameObject newArrow = Instantiate(arrow, shotPoint.position, shotPoint.rotation);
        if (!characterController.m_FacingRight)
        {
            newArrow.transform.eulerAngles = new Vector3(0, 0, 180);
        }
        playerAnim.SetBool("canShoot", false);
        audio.clip = bowAudio;
        audio.Play();
    }

    void Attack()
    {
        movementScript.isAttacking = true;
        playerAnim.SetInteger("AttackNum", attackNumber);
        playerAnim.SetBool("IsAttacking", true);
        attackNumber++;
    }

    public void SwordSound()
    {
        audio.clip = swordAudio;
        audio.Play();
    }
}
